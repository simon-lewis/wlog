package db

import (
	"bitbucket.org/simon-lewis/wlog/util"
	"database/sql"
	"fmt"
	"github.com/go-openapi/strfmt"
	_ "github.com/mattn/go-sqlite3"
)

var database *sql.DB

const version = 1

func InitDb(dbname string) {
	database, _ = sql.Open("sqlite3", fmt.Sprintf("./%s-v%d.db", dbname, version))
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS entries (entry_id INTEGER PRIMARY KEY, entry_time DATETIME, task TEXT)")
	statement.Exec()

	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS tasks (task_id INTEGER PRIMARY KEY, task TEXT)")
	statement.Exec()
}

func InsertEntry(entry Entry) {
	statement, _ := database.Prepare("INSERT INTO entries (entry_time, task) VALUES (?, ?)")
	_, err := statement.Exec(entry.Time, entry.Task)
	util.CheckErr(err)
	//rows,_ := result.RowsAffected()
	//lastId,_ := result.LastInsertId()
	//fmt.Printf("rows: %v, id: %v, error: %v", rows, lastId, err)
}

func InsertTask(task Task) {
	statement, _ := database.Prepare("INSERT INTO tasks (task) VALUES (?)")
	_, err := statement.Exec(task.Task)
	util.CheckErr(err)
}

func SelectEntriesByDate(date strfmt.Date) []Entry {

	// query
	queryString := `SELECT entry_id, entry_time, task 
    FROM entries
    WHERE date(entry_time,'localtime') = '` + date.String() + "'"
	rows, err := database.Query(queryString)
	util.CheckErr(err)

	var entries = make([]Entry, 0)

	for rows.Next() {
		var entry Entry
		err = rows.Scan(&entry.EntryId, &entry.Time, &entry.Task)
		util.CheckErr(err)
		entries = append(entries, entry)
	}

	rows.Close() //good habit to close

	return entries
}

func SelectAllEntries() []Entry {

	// query
	queryString := `SELECT entry_id, entry_time, task 
    FROM entries`
	rows, err := database.Query(queryString)
	util.CheckErr(err)

	var entries = make([]Entry, 0)

	for rows.Next() {
		var entry Entry
		err = rows.Scan(&entry.EntryId, &entry.Time, &entry.Task)
		util.CheckErr(err)
		entries = append(entries, entry)
	}

	rows.Close() //good habit to close

	return entries
}

func SelectTasks() []Task {
	// query
	rows, err := database.Query("SELECT task_id, task FROM tasks")
	util.CheckErr(err)

	var tasks = make([]Task, 0)

	for rows.Next() {
		var task Task
		err = rows.Scan(&task.TaskId, &task.Task)
		util.CheckErr(err)
		tasks = append(tasks, task)
	}
	return tasks
}

func DeleteOneEntry(entryId string) error {

	_, err := database.Exec("DELETE from entries where entry_id = " + entryId)
	return err
}
