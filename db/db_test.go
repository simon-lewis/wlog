package db

import (
	"fmt"
	"github.com/go-openapi/strfmt"
	"os"
	"testing"
	"time"
)

const (
	layoutISO  = "2006-01-02"
	layoutUS   = "January 2, 2006"
	layoutNZ   = "02Jan2006"
	layoutTime = "15:04:05"
)

func TestInsertEntries(t *testing.T) {
	os.Remove("./test.db")
	InitDb("test")
	addEntries()
	entries := SelectEntriesByDate(strfmt.Date(time.Now()))
	for rowNum, row := range entries {
		fmt.Printf("%v: %v %v %v %v\n", rowNum, row.EntryId, row.Date(), row.Time.Format(layoutTime), row.Task)
	}
}

func addEntries() {
	for i := 0; i < 10; i++ {
		var entry Entry
		entry.Task = fmt.Sprintf("test task %v", i)
		entry.Time = time.Now()
		InsertEntry(entry)
	}
}

func TestInsertTasks(t *testing.T) {
	os.Remove("./test.db")
	InitDb("test")
	addTasks()
	tasks := SelectTasks()
	for rowNum, row := range tasks {
		fmt.Printf("%v: %v %v\n", rowNum, row.TaskId, row.Task)
	}
}

func addTasks() {
	for i := 0; i < 5; i++ {
		var task Task
		task.Task = fmt.Sprintf("test task %v", i)
		InsertTask(task)
	}
}
