package db

import (
	"github.com/go-openapi/strfmt"
	"time"
)

const (
	TimeFormat = "15:04:05"
)

type Entry struct {
	EntryId int       `json:"entryId"`
	Time    time.Time `json:"time"`
	Task    string    `json:"task"`
}

type Task struct {
	TaskId int    `json:"taskId"`
	Task   string `json:"task"`
}

func (entry Entry) Date() strfmt.Date {
	return strfmt.Date(entry.Time)
}

func (entry Entry) TimeString() string {
	return entry.Time.Format(TimeFormat)
}
