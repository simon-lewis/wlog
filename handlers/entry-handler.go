package handlers

import (
	"bitbucket.org/simon-lewis/wlog/db"
	"bitbucket.org/simon-lewis/wlog/reports"
	"bitbucket.org/simon-lewis/wlog/util"
	"bytes"
	"fmt"
	"github.com/go-openapi/strfmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"
)

type EntryPage struct {
	ContextPath  string
	Date         strfmt.Date
	Entries      []db.Entry
	Tasks        []db.Task
	CurrentTime  func() string
	DailySummary map[string]string
}

const (
	TimeFormat = "15:04:05"
	DateFormat = "2006-01-02"
)

var (
	buf    bytes.Buffer
	logger = log.New(&buf, "logger: ", log.Lshortfile)
)

func EntryHandler(w http.ResponseWriter, r *http.Request) {

	deleteId, isDelete := r.URL.Query()["delete"]
	if isDelete {
		deleteOneEntry(deleteId[0])
	}

	if r.Method == http.MethodPost {
		saveFormData(r)
	}

	// check for the date
	date := getDate(r)

	entryPage := createEntryPage(date)

	templates := template.Must(template.ParseFiles("templates/entry-template.html"))
	if err := templates.ExecuteTemplate(w, "entry-template.html", entryPage); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func deleteOneEntry(entryId string) {
	db.DeleteOneEntry(entryId)
}

func createEntryPage(date strfmt.Date) EntryPage {
	entries := db.SelectEntriesByDate(date)
	reports.SortEntries(entries)
	//	entries := db.SelectAllEntries()
	tasks := db.SelectTasks()
	summaryRecords := reports.DailySummary(date)
	entryPage := EntryPage{
		ContextPath: util.ContextPath,
		Date:        date,
		Entries:     entries,
		Tasks:       tasks,
		CurrentTime: func() string {
			return time.Now().Format("15:04")
		},
		DailySummary: summaryRecords,
	}
	return entryPage
}

func saveFormData(r *http.Request) {

	// is there a new time / task ?
	entryTimeStr := r.FormValue("time")
	entryTask := r.FormValue("task")
	if len(entryTimeStr) > 0 && len(entryTask) > 0 {
		entryDateStr := r.FormValue("date")
		location := time.Local
		insertNewEntry(entryTimeStr, entryDateStr, location, entryTask)
	}

	// is there a new task to be added ?
	newTask := r.FormValue("newTask")
	if len(newTask) > 0 {
		task := db.Task{
			Task: newTask,
		}
		db.InsertTask(task)
	}
}

func insertNewEntry(entryTimeStr string, entryDateStr string, location *time.Location, entryTask string) {
	entryTime, _ := time.ParseInLocation(TimeFormat, entryTimeStr, location)
	entryDate, _ := time.ParseInLocation(DateFormat, entryDateStr, location)
	entryTime = time.Date(
		entryDate.Year(), entryDate.Month(), entryDate.Day(),
		entryTime.Hour(), entryTime.Minute(), entryTime.Second(), entryTime.Nanosecond(),
		location)
	newEntry := db.Entry{
		Time: entryTime,
		Task: entryTask,
	}
	db.InsertEntry(newEntry)
}

func getDate(request *http.Request) strfmt.Date {
	var date time.Time
	dateStr := request.URL.Query().Get("date")
	if len(dateStr) > 0 {
		var err error
		date, err = time.ParseInLocation("2006-01-02", dateStr, time.Local)
		if err != nil {
			logger.Printf("Error parsing date, '%s', using 'now' instead", dateStr)
			date = time.Now()
		}
	} else {
		date = time.Now()
	}
	offsetStr := request.URL.Query().Get("offset")
	if offsetStr == "1" || offsetStr == "-1" {
		date = offsetDate(date, offsetStr)
	}
	return strfmt.Date(date)
}

func offsetDate(date time.Time, offsetStr string) time.Time {
	dayOffset, err := strconv.Atoi(offsetStr)
	if err != nil {
		fmt.Sprintln("Error converting \"%v\" to integer", offsetStr)
		return date
	}
	newDate := date.AddDate(0, 0, dayOffset)
	return newDate
}
