package handlers

import (
	"bitbucket.org/simon-lewis/wlog/util"
	"html/template"
	"net/http"
	"time"
)

//Create a struct that holds information to be displayed in our HTML file
type Welcome struct {
	ContextPath string
	Name        string
	Time        string
}

func AboutHandler(w http.ResponseWriter, r *http.Request) {
	aboutMessage := "about wlog"
	w.Write([]byte(aboutMessage))
}

func WelcomeHandler(w http.ResponseWriter, r *http.Request) {
	//Instantiate a Welcome struct object and pass in some random information.
	//We shall get the name of the user as a query parameter from the URL
	welcome := Welcome{
		util.ContextPath,
		"Anonymous",
		time.Now().Format(time.Stamp),
	}

	//We tell Go exactly where we can find our html file. We ask Go to parse the html file (Notice
	// the relative path). We wrap it in a call to template.Must() which handles any errors
	// and halts if there are fatal errors
	templates := template.Must(template.ParseFiles("templates/welcome-template.html"))

	//Takes the name from the URL query e.g ?name=Martin, will set welcome.Name = Martin.
	if name := r.FormValue("name"); name != "" {
		welcome.Name = name
	}
	//If errors show an internal server error message
	//I also pass the welcome struct to the welcome-template.html file.
	if err := templates.ExecuteTemplate(w, "welcome-template.html", welcome); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
