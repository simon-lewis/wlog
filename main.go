package main

import (
	"bitbucket.org/simon-lewis/wlog/db"
	"bitbucket.org/simon-lewis/wlog/handlers"
	"bitbucket.org/simon-lewis/wlog/util"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func main() {

	db.InitDb("wlog")

	router := mux.NewRouter()
	setupEndpointMapping(router)

	fmt.Println(http.ListenAndServe(":9876", router))
}

func setupEndpointMapping(router *mux.Router) {
	router.PathPrefix(util.ContextPath + "/static/").Handler(http.StripPrefix(util.ContextPath+"/static/", http.FileServer(http.Dir("./static"))))
	router.HandleFunc(util.ContextPath, handlers.WelcomeHandler)
	router.HandleFunc(util.ContextPath+"/", handlers.WelcomeHandler)
	router.HandleFunc(util.ContextPath+"/about", handlers.AboutHandler).Methods("GET")
	router.HandleFunc(util.ContextPath+"/entry", handlers.EntryHandler).Methods("GET", "POST")
}
