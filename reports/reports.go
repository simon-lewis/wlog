package reports

import (
	"bitbucket.org/simon-lewis/wlog/db"
	"fmt"
	"github.com/go-openapi/strfmt"
	"sort"
	"time"
)

type EntryWithElapsed struct {
	Time    time.Time
	Task    string
	Elapsed time.Duration
}

func (entry EntryWithElapsed) Date() strfmt.Date {
	return strfmt.Date(entry.Time)
}

type TaskWithElapsed struct {
	Task    string
	Elapsed time.Duration
}

func DailySummary(date strfmt.Date) map[string]string {
	// fetch from the db
	entries := db.SelectEntriesByDate(date)
	//	entries := db.SelectAllEntries()
	// add an ending 'now' row
	nowEntry := db.Entry{
		Time: time.Now(),
		Task: "now",
	}
	entries = append(entries, nowEntry)

	// sort with most recent first
	SortEntries(entries)

	// compute the elapsed time between each entry
	rows := computeElapsedTime(entries)

	// aggregate the tasks / times, and return
	return createDailySummary(rows)
}

func SortEntries(rows []db.Entry) {
	// sort the rows by date/time
	sort.Slice(rows, func(i, j int) bool {
		return rows[i].Time.Before(rows[j].Time)
	})
}

func FormatDuration(d time.Duration) string {
	d = d.Round(time.Minute)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	return fmt.Sprintf("%dh %dm", h, m)
}

func createDailySummary(entriesWithElapsed []EntryWithElapsed) map[string]string {
	tasksList := make(map[string]time.Duration)
	for _, task := range entriesWithElapsed {
		if duration, found := tasksList[task.Task]; found {
			tasksList[task.Task] = duration + task.Elapsed
		} else {
			tasksList[task.Task] = task.Elapsed
		}
	}

	tasksListString := make(map[string]string)
	for task, duration := range tasksList {
		if task != "now" && task != "Finish" {
			durationString := FormatDuration(duration)
			tasksListString[task] = durationString
		}
	}
	return tasksListString
}

func computeElapsedTime(sortedEntries []db.Entry) []EntryWithElapsed {
	// assumes the slice is sorted in datetime order, most recent first
	var entriesWithElapsed = make([]EntryWithElapsed, 0)
	for i, _ := range sortedEntries {
		entryWithElapsed := constructElapsedEntry(sortedEntries, i)
		entriesWithElapsed = append(entriesWithElapsed, entryWithElapsed)
	}
	return entriesWithElapsed
}

func constructElapsedEntry(entries []db.Entry, rowNum int) EntryWithElapsed {
	entryWithElapsed := EntryWithElapsed{
		Time: entries[rowNum].Time,
		Task: entries[rowNum].Task,
	}
	if rowNum < len(entries)-1 {
		startTime := entries[rowNum].Time
		endTime := entries[rowNum+1].Time
		entryWithElapsed.Elapsed = endTime.Sub(startTime)
	}
	return entryWithElapsed
}
