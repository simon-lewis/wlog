package reports

import (
	"bitbucket.org/simon-lewis/wlog/db"
	"bitbucket.org/simon-lewis/wlog/util"
	"fmt"
	"os"
	"testing"
	"time"
)

func TestSortEntryList(t *testing.T) {
	os.Remove("./test-v1.db")
	db.InitDb("test")
	var entries = createList()
	for _, row := range entries {
		db.InsertEntry(row)
	}
	fmt.Println("list before sorting")
	PrintEntries(entries)
	SortEntries(entries)
	fmt.Println("list after sorting")
	PrintEntries(entries)
}

func TestElapsedTime(t *testing.T) {
	var entries = createList()
	SortEntries(entries)
	timedEntries := computeElapsedTime(entries)
	PrintTimesWithElapsed(timedEntries)
}

func PrintTimesWithElapsed(entries []EntryWithElapsed) {
	fmt.Print("ndx\tdate\t\ttime\t\ttask\telapsed\n")
	fmt.Print("===\t====\t\t====\t\t=======\t====\n")
	for i, entry := range entries {
		fmt.Printf("%2v\t%v\t%v\t%v\t%v\n", i, entry.Date(), entry.Time.Format("15:04:05"), entry.Task, entry.Elapsed)
	}
}

func createList() []db.Entry {
	entries := make([]db.Entry, 0)
	entries = append(entries, newEntry("task3", 9, 45))
	entries = append(entries, newEntry("task1", 8, 10))
	entries = append(entries, newEntry("task1", 9, 15))
	entries = append(entries, newEntry("end", 10, 5))
	entries = append(entries, newEntry("task2", 8, 30))
	return entries
}

func newEntry(task string, hour int, min int) db.Entry {
	s := fmt.Sprintf("2019-08-11T%02d:%02d:00.000Z", hour, min)
	t, err := time.Parse(time.RFC3339, s)
	util.CheckErr(err)
	e := db.Entry{
		Time: t,
		Task: task,
	}
	return e
}

func PrintEntries(entries []db.Entry) {
	fmt.Print("ndx\tid\tdate\t\ttime\t\ttask\n")
	fmt.Print("===\t==\t====\t\t====\t\t====\n")
	for i, entry := range entries {
		fmt.Printf("%2v\t%v\t%v\t%v\t%v\n", i, entry.EntryId, entry.Date(), entry.Time.Format("15:04:05"), entry.Task)
	}
}
