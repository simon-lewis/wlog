package util

const ContextPath = "/wlog"

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}
